package patterns.decorator;

import eu.telecomnancy.ui.MainWindow;

public class SwingAppDecorator {



    public static void main(String[] args) {
    	
        //C
    	DecoratorTemperatureSensor celsius = new TemperatureSensorCelsius();
        //F
    	DecoratorTemperatureSensor fahrenheit = new TemperatureSensorFahrenheit();
        //round
    	DecoratorTemperatureSensor round = new TemperatureSensorRound();
    	
    	
        new MainWindow(round);
        

    }

}
