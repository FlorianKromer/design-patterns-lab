package patterns.commande;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class CommandeUpdate extends CommandeAbstract implements ICommande{

	public CommandeUpdate(ISensor sensor) {
		super(sensor);
	}

	@Override
	public void Execute() throws SensorNotActivatedException {
		sensor.update();
	}

}