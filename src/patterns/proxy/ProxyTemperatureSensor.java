package patterns.proxy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LogLevel;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TemperatureSensor;

public class ProxyTemperatureSensor extends TemperatureSensor implements ISensor{



	@Override
	public void on() {
		this.log(LogLevel.INFO, "on()");
		super.on();

	}

	@Override
	public void off() {
		this.log(LogLevel.INFO, "off()");
		super.off();
		
	}

	@Override
	public boolean getStatus() {
		boolean b = super.getStatus();
		this.log(LogLevel.INFO, "Status() => "+b);
		return b;
	}

	@Override
	public void update() throws SensorNotActivatedException{
		this.log(LogLevel.INFO, "Update()");
		
		try {
			super.update();
		} catch (SensorNotActivatedException e) {
			this.log(LogLevel.ERROR, e.getMessage());
			throw e;
		}
	
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		double d = 0;
		try {
			d = super.getValue();
			this.log(LogLevel.INFO, "getvalue() => "+d);
		} catch (SensorNotActivatedException e) {
			this.log(LogLevel.ERROR, e.getMessage());
			throw e;
		}
		return d;
	}
	
	


	
}
