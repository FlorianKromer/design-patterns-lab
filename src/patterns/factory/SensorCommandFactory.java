package patterns.factory;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;

public class SensorCommandFactory extends SensorFactory {
    @Override
    public ISensor getSensor() {
        return new TemperatureSensor();
    }
}