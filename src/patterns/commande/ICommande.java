package patterns.commande;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public interface ICommande {

	public void Execute() throws SensorNotActivatedException;
}
