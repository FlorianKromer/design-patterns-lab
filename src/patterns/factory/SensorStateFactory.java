package patterns.factory;

import patterns.state.StateTemperatureSensor;
import eu.telecomnancy.sensor.ISensor;

public class SensorStateFactory extends SensorFactory {
    @Override
    public ISensor getSensor() {
        return new StateTemperatureSensor();
    }
}