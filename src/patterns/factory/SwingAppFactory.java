package patterns.factory;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingAppFactory {

    public static void main(String[] args) {
        ISensor sensor = SensorFactory.makeSensor();
        new MainWindow(sensor);

    }

}
