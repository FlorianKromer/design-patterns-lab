package patterns.decorator;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public class TemperatureSensorFahrenheit extends DecoratorTemperatureSensor{
	@Override
	public double getValue() throws SensorNotActivatedException {
		return super.getValue()*1.8 + 32;
	}
}
