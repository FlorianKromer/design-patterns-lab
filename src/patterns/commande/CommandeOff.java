package patterns.commande;

import eu.telecomnancy.sensor.ISensor;

public class CommandeOff extends CommandeAbstract implements ICommande{

	public CommandeOff(ISensor sensor) {
		super(sensor);
	}

	@Override
	public void Execute() {
		sensor.off();
	}

}