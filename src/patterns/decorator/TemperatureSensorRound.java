package patterns.decorator;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public class TemperatureSensorRound extends DecoratorTemperatureSensor{
	@Override
	public double getValue() throws SensorNotActivatedException {
		return Math.rint(super.getValue());
	}
}
