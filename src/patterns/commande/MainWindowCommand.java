package patterns.commande;

import java.awt.BorderLayout;
import java.util.Observable;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import eu.telecomnancy.sensor.ISensor;

public class MainWindowCommand extends JFrame{
	private ISensor sensor;
	private SensorViewCommand sensorView;

	public MainWindowCommand(ISensor sensor) {
		this.sensor = sensor;
		this.sensorView = new SensorViewCommand(this.sensor);

		((Observable) sensor).addObserver(this.sensorView);

		this.setLayout(new BorderLayout());
		this.add(this.sensorView, BorderLayout.CENTER);

		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}
}
