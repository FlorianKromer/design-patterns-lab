package patterns.adapter;

import java.util.Observable;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.LogLevel;
import eu.telecomnancy.sensor.SensorLogger;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class LegacyTemperatureSensorAdapter extends Observable implements
		ISensor {

	private LegacyTemperatureSensor lts;

	public LegacyTemperatureSensorAdapter(LegacyTemperatureSensor lts) {
		super();
		this.lts = lts;
	}

	@Override
	public void on() {
		if (!this.lts.getStatus()) {
			this.lts.onOff();
		}

	}

	@Override
	public void off() {
		if (this.lts.getStatus()) {
			this.lts.onOff();
		}
	}

	@Override
	public boolean getStatus() {
		return this.lts.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if (getStatus()) {
			setChanged();
			notifyObservers(this);
		} else
			throw new SensorNotActivatedException(
					"Sensor must be activated before acquiring new values.");
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		if (getStatus()) {
			return this.lts.getTemperature();
		} else
			throw new SensorNotActivatedException(
					"Sensor must be activated before acquiring new values.");
	}



}
