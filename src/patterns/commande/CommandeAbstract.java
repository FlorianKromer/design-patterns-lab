package patterns.commande;

import eu.telecomnancy.sensor.ISensor;

public abstract class CommandeAbstract implements ICommande{

	protected ISensor sensor;

	public CommandeAbstract(ISensor sensor) {
		this.sensor = sensor;
	}
	
}
