package patterns.adapter;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingAppAdapter {

	public static void main(String[] args) {
      LegacyTemperatureSensor lts = new LegacyTemperatureSensor();
      ISensor sensor = new LegacyTemperatureSensorAdapter(lts);
      new MainWindow(sensor);
	}

}
