package patterns.factory;

import patterns.proxy.ProxyTemperatureSensor;
import eu.telecomnancy.sensor.ISensor;

public class SensorProxyFactory extends SensorFactory {
    @Override
    public ISensor getSensor() {
        return new ProxyTemperatureSensor();
    }
}