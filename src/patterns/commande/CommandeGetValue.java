package patterns.commande;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class CommandeGetValue extends CommandeAbstract implements ICommande{

	public CommandeGetValue(ISensor sensor) {
		super(sensor);
	}

	@Override
	public void Execute() throws SensorNotActivatedException {
		double d = sensor.getValue();
		System.out.println("value: "+d);
	}

}