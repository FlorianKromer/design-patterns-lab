package patterns.proxy;

import patterns.state.StateTemperatureSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingAppProxy {

    public static void main(String[] args) {
        ISensor sensor = new ProxyTemperatureSensor();
        new MainWindow(sensor);
        

    }

}
