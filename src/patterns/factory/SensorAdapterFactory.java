package patterns.factory;

import patterns.adapter.LegacyTemperatureSensorAdapter;
import patterns.proxy.ProxyTemperatureSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;

public class SensorAdapterFactory extends SensorFactory {
    @Override
    public ISensor getSensor() {
        LegacyTemperatureSensor lts = new LegacyTemperatureSensor();

        return new LegacyTemperatureSensorAdapter(lts);
    }
}