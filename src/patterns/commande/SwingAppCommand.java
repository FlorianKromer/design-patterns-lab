package patterns.commande;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingAppCommand {
    public static void main(String[] args) {
        ISensor sensor = new TemperatureSensor();
        new MainWindowCommand(sensor);
        

    }
}
