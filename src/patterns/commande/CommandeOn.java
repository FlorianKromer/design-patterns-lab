package patterns.commande;

import eu.telecomnancy.sensor.ISensor;

public class CommandeOn extends CommandeAbstract implements ICommande{

	public CommandeOn(ISensor sensor) {
		super(sensor);
	}

	@Override
	public void Execute() {
		sensor.on();
	}

}
