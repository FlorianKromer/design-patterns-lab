package patterns.state;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingAppState {

    public static void main(String[] args) {
        ISensor sensor = new StateTemperatureSensor();
        new MainWindow(sensor);
        

    }

}
