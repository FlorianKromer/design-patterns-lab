package patterns.state;

import java.util.Observable;
import java.util.Random;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LogLevel;
import eu.telecomnancy.sensor.SensorLogger;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class StateTemperatureSensor extends Observable implements ISensor {
	State state;
	double value = 0;

	@Override
	public void on() {
		state = new StateOn();
	}

	@Override
	public void off() {
		state = new StateOff();
	}

	@Override
	public boolean getStatus() {
		return state.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		state.setValue((new Random()).nextDouble() * 100);
		setChanged();
		notifyObservers(this);
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return state.getValue();
	}

}
